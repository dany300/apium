package tests;

import java.net.MalformedURLException;

import org.junit.AfterClass;
import org.junit.BeforeClass;

import io.appium.java_client.AppiumDriver;
import utils.AndroidHelper;

/**
 * @author Daniel
 *
 */
public abstract class BaseTest {
    protected static AppiumDriver<?> driver;

    @BeforeClass
    public static void setUp() throws MalformedURLException {
	driver = AndroidHelper.setUpAppiumDriver();
    }

    @AfterClass
    public static void teardown() {
	// close the app
	driver.quit();
    }

    protected void logStep(String step) {
	System.out.println("[STEP]" + step);
    }

    protected void logTestName(String step) {
	System.out.println("[TEST NAME]" + step);
    }
}
