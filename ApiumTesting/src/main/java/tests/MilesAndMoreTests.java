package tests;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import pages.HomePage;
import pages.LeftBurgerMenuPage;
import pages.LoginPage;
import pages.MileageCalculatorPage;
import pages.MyProfileAndSettingsPage;
import pages.PersonalInformationPage;

/**
 * @author Daniel
 *
 */
public class MilesAndMoreTests extends BaseTest {
    private LoginPage loginPage;
    private HomePage homePage;
    private LeftBurgerMenuPage leftBurgerMenu;
    private MyProfileAndSettingsPage myProfileAndSettings;
    private PersonalInformationPage personalInformation;

    private MileageCalculatorPage mileageCalculatorPage;

    @Test
    public void testEditProfile() throws Exception {
	logTestName("Edit profile");
	logStep("1.Login with a manually created user");
	homePage = loginPage.login("dany300", "Test300!");

	logStep("2.Click on the top left burger menu");
	leftBurgerMenu = homePage.openLeftBurgerMenu();

	logStep("3.Go to �My Profile & Settings� -> Personal Information");
	myProfileAndSettings = leftBurgerMenu.openMyProfileAndSettings();//

	logStep("4.Click on �Edit address�");
	personalInformation = myProfileAndSettings.openPersonalInformation();
	personalInformation.editAddress();
	personalInformation.insertPassword("Test300!");

	logStep("4.a.Add tests for both valid data and invalid data input (check that the invalid field is highlighted)");
	logStep("4.a.1.Valid data");
	personalInformation.editAndSaveCity("newCity");
	assertTrue("newCity".equals(personalInformation.getCityValue()));
	logStep("4.a.2.Invalid data input (check that the invalid field is highlighted)");
	personalInformation.editAndSaveCity("");
//	TODO field is highlighted
	personalInformation.clickX();
	assertTrue(!"newCity".equals(personalInformation.getCityValue()));

	logStep("4.b.Verify that the changed data is discarded when the user taps on X after some fields were changed ");
	personalInformation.editCity("discardedValue");
	personalInformation.clickX();
	assertTrue("newCity".equals(personalInformation.getCityValue()));

	logStep("4.c.Verify that after too much inactivity in the �Edit address� screen (2min) the user is prompted with an error message");
    }

    @Test
    public void testMileageCalculator() throws Exception {
	logTestName("Mileage calculator");
	logStep("1.Login with a manually created user");
	homePage = loginPage.login("dany300", "Test300!");

	logStep("2.Tap on the top left burger menu");
	leftBurgerMenu = homePage.openLeftBurgerMenu();//

	logStep("3.Go to �Mileage calculator� -> Calculate miles");
	mileageCalculatorPage = leftBurgerMenu.openMileageCalculator();//
	mileageCalculatorPage.calculateMiles();

	logStep("4.Add test for mileage calculation when entering valid �Your journey� section data, but without �Ticket details� data");
	logStep("4.a.Tap on �Edit� ");
	logStep("4.b.Enter valid data (ex: Berlin, 07.03.2018, LH, Lufthansa, C, Paris)");
	logStep("4.c.Tap on Done");
	logStep("4.d.Tap on Calculate miles");
	logStep("4.e.Check the existence of Frequent traveler, Senator and HON Circle values in the result ");
	logStep("5.Tap on New calculation and use invalid combination");
	logStep("5.a.Check that an error message is displayed");
	logStep("6.Re-execute the steps on #4 with valid data but this time fill in the �Ticket details� fields and tap again on Calculate miles");
	logStep("6.a.Tap on Request miles");
	logStep("6.b.Tap on �Go to the online form�");
	logStep("6.c.Tap on the top right button for opening the web page in the browser ");
    }
}
