/**
 * 
 */
package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import io.appium.java_client.android.AndroidDriver;

/**
 * @author Daniel
 *
 */
public class MyProfileAndSettingsPage extends AbstractPage {
    private By personalInformationLocator = By.id("NOT_SET");

    public MyProfileAndSettingsPage(AndroidDriver<WebElement> driver) {
	super(driver);
    }

    public PersonalInformationPage openPersonalInformation() {
	clickElement(personalInformationLocator);
	return new PersonalInformationPage(driver);
    }

}
