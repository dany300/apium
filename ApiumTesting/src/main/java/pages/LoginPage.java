/**
 * 
 */
package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import io.appium.java_client.android.AndroidDriver;

/**
 * @author Daniel
 *
 */
public class LoginPage extends AbstractPage {
    private By usernameTfdLocator = By.id("NOT_SET");
    private By passwordTfdLocator = By.id("NOT_SET");
    private By signInBtnLocator = By.id("com.plannet.milesandmoreapp:id/button_login");

    public LoginPage(AndroidDriver<WebElement> driver) {
	super(driver);
    }

    public HomePage login(String username, String password) {
	insertElementValue(usernameTfdLocator, username);
	insertElementValue(passwordTfdLocator, password);
	clickElement(signInBtnLocator);
	return new HomePage(driver);
    }
}