/**
 * 
 */
package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import io.appium.java_client.android.AndroidDriver;

/**
 * @author Daniel
 *
 */
public class PersonalInformationPage extends AbstractPage {
    private By editAddressBtnLocator = By.id("NOT_SET");
    private By passwordTfdLocator = By.id("NOT_SET");
    private By cityLocator = By.id("NOT_SET");
    private By saveEditLocator = By.id("NOT_SET");
    private By xLocator = By.id("NOT_SET");

    public PersonalInformationPage(AndroidDriver<WebElement> driver) {
	super(driver);
    }

    public void editAddress() {
	clickElement(editAddressBtnLocator);
    }

    public void insertPassword(String password) {
	insertElementValue(passwordTfdLocator, password);
    }

    public void editCity(String newValue) {
	clearAndInsertElementValue(cityLocator, newValue);
	clickElement(saveEditLocator);
    }

    public void editAndSaveCity(String newValue) {
	clearAndInsertElementValue(cityLocator, newValue);
	clickElement(saveEditLocator);
    }

    public void clickX() {
	clickElement(xLocator);
    }

    public String getCityValue() {
	return getElementFromLocator(cityLocator).getText();
    }

}
