/**
 * 
 */
package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import io.appium.java_client.android.AndroidDriver;

/**
 * @author Daniel
 *
 */
public class AbstractPage {
    protected AndroidDriver<WebElement> driver;

    public AbstractPage(AndroidDriver<WebElement> driver) {
	this.driver = driver;
    }

    protected void logAction(String action) {
	System.out.println("[Action]" + action);
    }

    protected WebElement getElementFromLocator(By locator) {
	return driver.findElement(locator);
    }

    protected void clickElement(By locator) {
	logAction("Click on locator: " + locator);
	getElementFromLocator(locator).click();
    }

    protected void insertElementValue(By locator, String value) {
	logAction("Insert value: " + value + " in element with locator " + locator);
	getElementFromLocator(locator).sendKeys(value);
    }

    protected void clearAndInsertElementValue(By locator, String value) {
	logAction("Clear value in element with locator " + locator);
	getElementFromLocator(locator).clear();
	insertElementValue(locator, value);
    }

}
