/**
 * 
 */
package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import io.appium.java_client.android.AndroidDriver;

/**
 * @author Daniel
 *
 */
public class LeftBurgerMenuPage extends AbstractPage {
    private By myProfileAndSettingsLocator = By.id("NOT_SET");
    private By mileageCalculatorLocator = By.id("NOT_SET");

    public LeftBurgerMenuPage(AndroidDriver<WebElement> driver) {
	super(driver);
    }

    public MyProfileAndSettingsPage openMyProfileAndSettings() {
	clickElement(myProfileAndSettingsLocator);
	return new MyProfileAndSettingsPage(driver);
    }

    public MileageCalculatorPage openMileageCalculator() {
	clickElement(mileageCalculatorLocator);
	return new MileageCalculatorPage(driver);
    }

}
