/**
 * 
 */
package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import io.appium.java_client.android.AndroidDriver;

/**
 * @author Daniel
 *
 */
public class HomePage extends AbstractPage {
    private By leftBurgerMenuLocator = By.id("NOT_SET");

    public HomePage(AndroidDriver<WebElement> driver) {
	super(driver);
    }

    public LeftBurgerMenuPage openLeftBurgerMenu() {
	clickElement(leftBurgerMenuLocator);
	return new LeftBurgerMenuPage(driver);
    }

}
