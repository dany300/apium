package utils;

import java.util.Properties;

/**
 * @author Daniel
 *
 */
public abstract class PropertiesHelper {
    static final String apiumConfig = "apium.properties";

    public static Properties getPropertes(String propFile) {
	Properties prop = new Properties();
	try {
	    prop.load(ClassLoader.getSystemResourceAsStream(propFile));
	} catch (Exception e) {
	    e.printStackTrace(System.out);
	}
	return prop;
    }

    public static Properties getPropertes() {
	return getPropertes(apiumConfig);
    }

    public static String getProperty(String key) {
	try {
	    return getPropertes().getProperty(key);
	} catch (Exception e) {
	    e.printStackTrace(System.out);
	}
	return "";
    }
}
