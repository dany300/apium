package utils;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.remote.DesiredCapabilities;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.remote.MobileCapabilityType;

/**
 * @author Daniel
 *
 */
public abstract class AndroidHelper {
    public static AppiumDriver<?> setUpAppiumDriver() throws MalformedURLException {
	// Set up desired capabilities
	DesiredCapabilities capabilities = new DesiredCapabilities();
	capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME,
		PropertiesHelper.getProperty("apium.capabilities.general.platformName"));
	capabilities.setCapability(MobileCapabilityType.VERSION,
		PropertiesHelper.getProperty("apium.capabilities.general.platformVersion"));
	capabilities.setCapability(MobileCapabilityType.DEVICE_NAME,
		PropertiesHelper.getProperty("apium.capabilities.general.deviceName"));
	// This package name of your app (you can get it from apk info app)
	capabilities.setCapability(MobileCapabilityType.APP,
		PropertiesHelper.getProperty("apium.capabilities.general.app"));
	capabilities.setCapability("appPackage", PropertiesHelper.getProperty("apium.capabilities.android.appPackage"));
	// This is Launcher activity of your app (you can get it from apk info app)
	capabilities.setCapability("appActivity",
		PropertiesHelper.getProperty("apium.capabilities.android.appActivity"));
	return new AppiumDriver<MobileElement>(new URL(PropertiesHelper.getProperty("apium.server.URL")), capabilities);
    }
}
